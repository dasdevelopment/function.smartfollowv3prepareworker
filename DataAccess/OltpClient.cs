﻿using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccess
{
    public interface IOltpClient
    {
        public Task<Lead> GetLead(Guid lead_id);
        public Task<Franchise_Consumer_Txn> GetFranchiseConsumerTxn(Guid lead_id);
        public Task<Smart_Quote> GetSmartQuote(Guid lead_id);
        public Task LeadHistoryUpsert(Guid lead_id, string desc1, string desc2);
    }

    public class OltpClient : IOltpClient
    {
        private readonly string _connectionString;

        public OltpClient(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<Lead> GetLead(Guid lead_id)
        {
            using var context = new OltpContext(_connectionString);

                var query = @$"
                  select l.*, fr.full_name 'full_name', fr.email 'franchiseRepEmail'  from lead l with(Nolock)
                left join franchise_consumer_txn fct with (nolock) on (l.lead_id = fct.txn_id and fct.type = 'Lead')  
                  left join franchise_consumer fc with (nolock) on (fct.franchise_consumer_id = fc.franchise_consumer_id)  
                  left join franchise_rep fr with (nolock) on (fc.franchise_rep_id = fr.franchise_rep_id)  

                 where lead_id='{lead_id}'
";

                return await context.lead.FromSqlRaw(query).FirstOrDefaultAsync().ConfigureAwait(false);
        }

        public async Task<Franchise_Consumer_Txn> GetFranchiseConsumerTxn(Guid lead_id)
        {
            using var context = new OltpContext(_connectionString);

            return await context.franchise_consumer_txn.Where(x => x.txn_id == lead_id).FirstOrDefaultAsync().ConfigureAwait(false);
        }

        public async Task<Smart_Quote> GetSmartQuote(Guid lead_id)
        {
            using var context = new OltpContext(_connectionString);

            return await context.smart_quote.Where(x => x.lead_id == lead_id).FirstOrDefaultAsync().ConfigureAwait(false);
        }

        public async Task LeadHistoryUpsert(Guid lead_id, string desc1, string desc2)
        {
            var context = new OltpContext(_connectionString);
            var date = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Pacific Standard Time");
            var proc = $"exec lead_history_upsert @lead_history_id='{Guid.NewGuid()}', @lead_id='{lead_id}', @history_date='{date}', @type='SmartFollow', @logon='System', @description_1='{desc1}', @description_2='{desc2}';";

            await context.Database.ExecuteSqlRawAsync(proc);
        }
    }
}
