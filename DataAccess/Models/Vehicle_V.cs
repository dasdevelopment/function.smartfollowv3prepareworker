﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataAccess.Models
{
    public class Vehicle_V
    {
        [Key]
        public int make_id { get; set; }
        public string make { get; set; }
        public string make_plural { get; set; }
        public string year { get; set; }
        public string model { get; set; }
        public int vehicle_id { get; set; }
        public DateTime? created_date { get; set; }
        public int? model_id { get; set; }
        public string status { get; set; }
        public string source { get; set; }
        public string vehicle_number { get; set; }
        public string name { get; set; }
        public string trim { get; set; }
        public string vin { get; set; }
        public string stock_number { get; set; }
        public Decimal? invoice { get; set; }
        public Decimal? msrp { get; set; }
        public string vehicle_image_html { get; set; }
        public string manufacturer_code { get; set; }
        public string body_style { get; set; }
        public DateTime? last_updated_date { get; set; }
        public string last_updated_by { get; set; }
        public bool? use_employee_price { get; set; }
        public string image_url { get; set; }
        public string vehicle_segment { get; set; }
        public string duty_type { get; set; }
        public string vehicle_option { get; set; }
        public string vehicle_option_code_list { get; set; }
        public string photo_url { get; set; }
        public string photo_1_url { get; set; }
        public string photo_2_url { get; set; }
        public Decimal? DestinationCharge { get; set; }
        public string transmission { get; set; }
    }
}
