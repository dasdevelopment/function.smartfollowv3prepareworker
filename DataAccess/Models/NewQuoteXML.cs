﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Models
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class new_quotes
    {

        private new_quotesNew_vehicle_quote[] new_vehicle_quoteField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("new_vehicle_quote")]
        public new_quotesNew_vehicle_quote[] new_vehicle_quote
        {
            get
            {
                return this.new_vehicle_quoteField;
            }
            set
            {
                this.new_vehicle_quoteField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class new_quotesNew_vehicle_quote
    {

        private string vinField;

        private uint stock_numberField;

        private uint vehicle_idField;

        private ushort yearField;

        private string makeField;

        private string modelField;

        private string trimField;

        private string autodata_trimField;

        private string manufacturer_codeField;

        private object invoiceField;

        private string msrpField;

        private string final_priceField;

        private object lease_offer_expiration_dateField;

        private string statusField;

        private decimal final_price_moneyField;

        private byte used_vehicles_shown_maxField;

        private string selection_reasonField;

        private string vehicle_blob_urlField;

        private string inventory_blob_urlField;

        private ushort responselogix_region_idField;

        private object cash_backField;

        private object incentive_rate_24Field;

        private object incentive_rate_36Field;

        private object incentive_rate_48Field;

        private object incentive_rate_60Field;

        private object incentive_rate_72Field;

        private System.DateTime incentive_expiration_dateField;

        private System.DateTime cash_back_expiration_dateField;

        private string source_inventory_urlField;

        private new_quotesNew_vehicle_quoteImg[] imagesField;

        private object acodesField;

        private new_quotesNew_vehicle_quoteOption[] categorized_optionsField;

        private uint customer_idField;

        private string customer_guidField;

        private string typeField;

        private string date_in_stockField;

        private string body_styleField;

        private string standard_bodyField;

        private byte doorsField;

        private string exterior_colorField;

        private string interior_colorField;

        private ushort homenet_inventory_msrpField;

        private ushort homenet_inventory_final_priceField;

        private object monthly_lease_amountField;

        private object lease_down_paymentField;

        private object annual_mileageField;

        private object excess_mileage_feeField;

        private object lease_term_monthsField;

        private string lease_disclaimerField;

        private string photo_urlField;

        private string photo_1_urlField;

        private string photo_2_urlField;

        private byte mileageField;

        private byte engine_cylindersField;

        private string engine_displacementField;

        private string transmissionField;

        private byte is_certifiedField;

        private object epa_cityField;

        private object epa_highwayField;

        private decimal wheel_baseField;

        private string engineField;

        private string rooftop_nameField;

        private string last_updated_dateField;

        private string optionsField;

        private ushort book_value_moneyField;

        private string book_value_displayField;

        private string customer_inventory_sourceField;

        private object customer_inventory_quoted_priceField;

        private object customer_inventory_msrpField;

        private ushort price_after_markupField;

        private string vehicle_image_urlField;

        private string vehicle_image_1_urlField;

        private string vehicle_image_2_urlField;

        private string source_vehicle_urlField;

        private string make_pluralField;

        private string vehicle_numberField;

        /// <remarks/>
        public string vin
        {
            get
            {
                return this.vinField;
            }
            set
            {
                this.vinField = value;
            }
        }

        /// <remarks/>
        public uint stock_number
        {
            get
            {
                return this.stock_numberField;
            }
            set
            {
                this.stock_numberField = value;
            }
        }

        /// <remarks/>
        public uint vehicle_id
        {
            get
            {
                return this.vehicle_idField;
            }
            set
            {
                this.vehicle_idField = value;
            }
        }

        /// <remarks/>
        public ushort year
        {
            get
            {
                return this.yearField;
            }
            set
            {
                this.yearField = value;
            }
        }

        /// <remarks/>
        public string make
        {
            get
            {
                return this.makeField;
            }
            set
            {
                this.makeField = value;
            }
        }

        /// <remarks/>
        public string model
        {
            get
            {
                return this.modelField;
            }
            set
            {
                this.modelField = value;
            }
        }

        /// <remarks/>
        public string trim
        {
            get
            {
                return this.trimField;
            }
            set
            {
                this.trimField = value;
            }
        }

        /// <remarks/>
        public string autodata_trim
        {
            get
            {
                return this.autodata_trimField;
            }
            set
            {
                this.autodata_trimField = value;
            }
        }

        /// <remarks/>
        public string manufacturer_code
        {
            get
            {
                return this.manufacturer_codeField;
            }
            set
            {
                this.manufacturer_codeField = value;
            }
        }

        /// <remarks/>
        public object invoice
        {
            get
            {
                return this.invoiceField;
            }
            set
            {
                this.invoiceField = value;
            }
        }

        /// <remarks/>
        public string msrp
        {
            get
            {
                return this.msrpField;
            }
            set
            {
                this.msrpField = value;
            }
        }

        /// <remarks/>
        public string final_price
        {
            get
            {
                return this.final_priceField;
            }
            set
            {
                this.final_priceField = value;
            }
        }

        /// <remarks/>
        public object lease_offer_expiration_date
        {
            get
            {
                return this.lease_offer_expiration_dateField;
            }
            set
            {
                this.lease_offer_expiration_dateField = value;
            }
        }

        /// <remarks/>
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public decimal final_price_money
        {
            get
            {
                return this.final_price_moneyField;
            }
            set
            {
                this.final_price_moneyField = value;
            }
        }

        /// <remarks/>
        public byte used_vehicles_shown_max
        {
            get
            {
                return this.used_vehicles_shown_maxField;
            }
            set
            {
                this.used_vehicles_shown_maxField = value;
            }
        }

        /// <remarks/>
        public string selection_reason
        {
            get
            {
                return this.selection_reasonField;
            }
            set
            {
                this.selection_reasonField = value;
            }
        }

        /// <remarks/>
        public string vehicle_blob_url
        {
            get
            {
                return this.vehicle_blob_urlField;
            }
            set
            {
                this.vehicle_blob_urlField = value;
            }
        }

        /// <remarks/>
        public string inventory_blob_url
        {
            get
            {
                return this.inventory_blob_urlField;
            }
            set
            {
                this.inventory_blob_urlField = value;
            }
        }

        /// <remarks/>
        public ushort responselogix_region_id
        {
            get
            {
                return this.responselogix_region_idField;
            }
            set
            {
                this.responselogix_region_idField = value;
            }
        }

        /// <remarks/>
        public object cash_back
        {
            get
            {
                return this.cash_backField;
            }
            set
            {
                this.cash_backField = value;
            }
        }

        /// <remarks/>
        public object incentive_rate_24
        {
            get
            {
                return this.incentive_rate_24Field;
            }
            set
            {
                this.incentive_rate_24Field = value;
            }
        }

        /// <remarks/>
        public object incentive_rate_36
        {
            get
            {
                return this.incentive_rate_36Field;
            }
            set
            {
                this.incentive_rate_36Field = value;
            }
        }

        /// <remarks/>
        public object incentive_rate_48
        {
            get
            {
                return this.incentive_rate_48Field;
            }
            set
            {
                this.incentive_rate_48Field = value;
            }
        }

        /// <remarks/>
        public object incentive_rate_60
        {
            get
            {
                return this.incentive_rate_60Field;
            }
            set
            {
                this.incentive_rate_60Field = value;
            }
        }

        /// <remarks/>
        public object incentive_rate_72
        {
            get
            {
                return this.incentive_rate_72Field;
            }
            set
            {
                this.incentive_rate_72Field = value;
            }
        }

        /// <remarks/>
        public System.DateTime incentive_expiration_date
        {
            get
            {
                return this.incentive_expiration_dateField;
            }
            set
            {
                this.incentive_expiration_dateField = value;
            }
        }

        /// <remarks/>
        public System.DateTime cash_back_expiration_date
        {
            get
            {
                return this.cash_back_expiration_dateField;
            }
            set
            {
                this.cash_back_expiration_dateField = value;
            }
        }

        /// <remarks/>
        public string source_inventory_url
        {
            get
            {
                return this.source_inventory_urlField;
            }
            set
            {
                this.source_inventory_urlField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("img", IsNullable = false)]
        public new_quotesNew_vehicle_quoteImg[] images
        {
            get
            {
                return this.imagesField;
            }
            set
            {
                this.imagesField = value;
            }
        }

        /// <remarks/>
        public object acodes
        {
            get
            {
                return this.acodesField;
            }
            set
            {
                this.acodesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("option", IsNullable = false)]
        public new_quotesNew_vehicle_quoteOption[] categorized_options
        {
            get
            {
                return this.categorized_optionsField;
            }
            set
            {
                this.categorized_optionsField = value;
            }
        }

        /// <remarks/>
        public uint customer_id
        {
            get
            {
                return this.customer_idField;
            }
            set
            {
                this.customer_idField = value;
            }
        }

        /// <remarks/>
        public string customer_guid
        {
            get
            {
                return this.customer_guidField;
            }
            set
            {
                this.customer_guidField = value;
            }
        }

        /// <remarks/>
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string date_in_stock
        {
            get
            {
                return this.date_in_stockField;
            }
            set
            {
                this.date_in_stockField = value;
            }
        }

        /// <remarks/>
        public string body_style
        {
            get
            {
                return this.body_styleField;
            }
            set
            {
                this.body_styleField = value;
            }
        }

        /// <remarks/>
        public string standard_body
        {
            get
            {
                return this.standard_bodyField;
            }
            set
            {
                this.standard_bodyField = value;
            }
        }

        /// <remarks/>
        public byte doors
        {
            get
            {
                return this.doorsField;
            }
            set
            {
                this.doorsField = value;
            }
        }

        /// <remarks/>
        public string exterior_color
        {
            get
            {
                return this.exterior_colorField;
            }
            set
            {
                this.exterior_colorField = value;
            }
        }

        /// <remarks/>
        public string interior_color
        {
            get
            {
                return this.interior_colorField;
            }
            set
            {
                this.interior_colorField = value;
            }
        }

        /// <remarks/>
        public ushort homenet_inventory_msrp
        {
            get
            {
                return this.homenet_inventory_msrpField;
            }
            set
            {
                this.homenet_inventory_msrpField = value;
            }
        }

        /// <remarks/>
        public ushort homenet_inventory_final_price
        {
            get
            {
                return this.homenet_inventory_final_priceField;
            }
            set
            {
                this.homenet_inventory_final_priceField = value;
            }
        }

        /// <remarks/>
        public object monthly_lease_amount
        {
            get
            {
                return this.monthly_lease_amountField;
            }
            set
            {
                this.monthly_lease_amountField = value;
            }
        }

        /// <remarks/>
        public object lease_down_payment
        {
            get
            {
                return this.lease_down_paymentField;
            }
            set
            {
                this.lease_down_paymentField = value;
            }
        }

        /// <remarks/>
        public object annual_mileage
        {
            get
            {
                return this.annual_mileageField;
            }
            set
            {
                this.annual_mileageField = value;
            }
        }

        /// <remarks/>
        public object excess_mileage_fee
        {
            get
            {
                return this.excess_mileage_feeField;
            }
            set
            {
                this.excess_mileage_feeField = value;
            }
        }

        /// <remarks/>
        public object lease_term_months
        {
            get
            {
                return this.lease_term_monthsField;
            }
            set
            {
                this.lease_term_monthsField = value;
            }
        }

        /// <remarks/>
        public string lease_disclaimer
        {
            get
            {
                return this.lease_disclaimerField;
            }
            set
            {
                this.lease_disclaimerField = value;
            }
        }

        /// <remarks/>
        public string photo_url
        {
            get
            {
                return this.photo_urlField;
            }
            set
            {
                this.photo_urlField = value;
            }
        }

        /// <remarks/>
        public string photo_1_url
        {
            get
            {
                return this.photo_1_urlField;
            }
            set
            {
                this.photo_1_urlField = value;
            }
        }

        /// <remarks/>
        public string photo_2_url
        {
            get
            {
                return this.photo_2_urlField;
            }
            set
            {
                this.photo_2_urlField = value;
            }
        }

        /// <remarks/>
        public byte mileage
        {
            get
            {
                return this.mileageField;
            }
            set
            {
                this.mileageField = value;
            }
        }

        /// <remarks/>
        public byte engine_cylinders
        {
            get
            {
                return this.engine_cylindersField;
            }
            set
            {
                this.engine_cylindersField = value;
            }
        }

        /// <remarks/>
        public string engine_displacement
        {
            get
            {
                return this.engine_displacementField;
            }
            set
            {
                this.engine_displacementField = value;
            }
        }

        /// <remarks/>
        public string transmission
        {
            get
            {
                return this.transmissionField;
            }
            set
            {
                this.transmissionField = value;
            }
        }

        /// <remarks/>
        public byte is_certified
        {
            get
            {
                return this.is_certifiedField;
            }
            set
            {
                this.is_certifiedField = value;
            }
        }

        /// <remarks/>
        public object epa_city
        {
            get
            {
                return this.epa_cityField;
            }
            set
            {
                this.epa_cityField = value;
            }
        }

        /// <remarks/>
        public object epa_highway
        {
            get
            {
                return this.epa_highwayField;
            }
            set
            {
                this.epa_highwayField = value;
            }
        }

        /// <remarks/>
        public decimal wheel_base
        {
            get
            {
                return this.wheel_baseField;
            }
            set
            {
                this.wheel_baseField = value;
            }
        }

        /// <remarks/>
        public string engine
        {
            get
            {
                return this.engineField;
            }
            set
            {
                this.engineField = value;
            }
        }

        /// <remarks/>
        public string rooftop_name
        {
            get
            {
                return this.rooftop_nameField;
            }
            set
            {
                this.rooftop_nameField = value;
            }
        }

        /// <remarks/>
        public string last_updated_date
        {
            get
            {
                return this.last_updated_dateField;
            }
            set
            {
                this.last_updated_dateField = value;
            }
        }

        /// <remarks/>
        public string options
        {
            get
            {
                return this.optionsField;
            }
            set
            {
                this.optionsField = value;
            }
        }

        /// <remarks/>
        public ushort book_value_money
        {
            get
            {
                return this.book_value_moneyField;
            }
            set
            {
                this.book_value_moneyField = value;
            }
        }

        /// <remarks/>
        public string book_value_display
        {
            get
            {
                return this.book_value_displayField;
            }
            set
            {
                this.book_value_displayField = value;
            }
        }

        /// <remarks/>
        public string customer_inventory_source
        {
            get
            {
                return this.customer_inventory_sourceField;
            }
            set
            {
                this.customer_inventory_sourceField = value;
            }
        }

        /// <remarks/>
        public object customer_inventory_quoted_price
        {
            get
            {
                return this.customer_inventory_quoted_priceField;
            }
            set
            {
                this.customer_inventory_quoted_priceField = value;
            }
        }

        /// <remarks/>
        public object customer_inventory_msrp
        {
            get
            {
                return this.customer_inventory_msrpField;
            }
            set
            {
                this.customer_inventory_msrpField = value;
            }
        }

        /// <remarks/>
        public ushort price_after_markup
        {
            get
            {
                return this.price_after_markupField;
            }
            set
            {
                this.price_after_markupField = value;
            }
        }

        /// <remarks/>
        public string vehicle_image_url
        {
            get
            {
                return this.vehicle_image_urlField;
            }
            set
            {
                this.vehicle_image_urlField = value;
            }
        }

        /// <remarks/>
        public string vehicle_image_1_url
        {
            get
            {
                return this.vehicle_image_1_urlField;
            }
            set
            {
                this.vehicle_image_1_urlField = value;
            }
        }

        /// <remarks/>
        public string vehicle_image_2_url
        {
            get
            {
                return this.vehicle_image_2_urlField;
            }
            set
            {
                this.vehicle_image_2_urlField = value;
            }
        }

        /// <remarks/>
        public string source_vehicle_url
        {
            get
            {
                return this.source_vehicle_urlField;
            }
            set
            {
                this.source_vehicle_urlField = value;
            }
        }

        /// <remarks/>
        public string make_plural
        {
            get
            {
                return this.make_pluralField;
            }
            set
            {
                this.make_pluralField = value;
            }
        }

        /// <remarks/>
        public string vehicle_number
        {
            get
            {
                return this.vehicle_numberField;
            }
            set
            {
                this.vehicle_numberField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class new_quotesNew_vehicle_quoteImg
    {

        private string urlField;

        private byte orderField;

        private bool orderFieldSpecified;

        private string categoryField;

        private string descriptionField;

        /// <remarks/>
        public string url
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
            }
        }

        /// <remarks/>
        public byte order
        {
            get
            {
                return this.orderField;
            }
            set
            {
                this.orderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool orderSpecified
        {
            get
            {
                return this.orderFieldSpecified;
            }
            set
            {
                this.orderFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string category
        {
            get
            {
                return this.categoryField;
            }
            set
            {
                this.categoryField = value;
            }
        }

        /// <remarks/>
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class new_quotesNew_vehicle_quoteOption
    {

        private string categoryField;

        private string descriptionField;

        /// <remarks/>
        public string category
        {
            get
            {
                return this.categoryField;
            }
            set
            {
                this.categoryField = value;
            }
        }

        /// <remarks/>
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }
    }


}
