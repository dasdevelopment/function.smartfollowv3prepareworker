﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataAccess.Models
{
    public class Franchise_PL
    {
        [Key]
        public int franchise_pl_id { get; set; }
        public DateTime created_date { get; set; }
        public int franchise_id { get; set; }
        public string site { get; set; }
        public string name { get; set; }
        public string value { get; set; }
    }
}
