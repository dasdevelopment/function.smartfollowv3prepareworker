﻿using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models
{
    public class FranchiseRep
    {
        [Key]
        public int contact_id { get; set; }
        public string full_name { get; set; }
        public string title { get; set; }
        public string business_phone { get; set; }
        public string business_extension { get; set; }
        public string mobile_phone { get; set; }
        public string photo_url { get; set; }
        public short customer_contact_number { get; set; }
        public string email { get; set; }
    }
}
