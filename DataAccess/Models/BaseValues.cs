﻿namespace DataAccess.Models
{
    public static class BaseValues
    {
        /* SendEmailQueueMessage Example
<txn>
<txn_id>ace8b820-5d8f-469f-b649-58ed703cdc8e</txn_id>
<email_url>http://productionstoragerl.blob.core.windows.net/temp-20201007/emailqueue.sendmessage.d6d56d3d-2bf9-452f-80f2-6d61a8a22f8e.xml</email_url>
<queue_number>2</queue_number>
</txn>
        */
        public const string SendEmailQueueMessage =
@"<txn>
  <txn_id>{0}</txn_id>
  <email_url>{1}</email_url>
  <queue_number>{2}</queue_number>
</txn>";


//////////////////////////////////////////////////////
        public const string SendEmailTempBlob =
@"<txn>
  <email_bcc />
  <email_body_html>{0}</email_body_html>
  <email_body_html_url />
  <email_body_text></email_body_text>
  <email_body_text_url />
  <email_cc />
  <email_from>{1}</email_from>
  <email_reply_to>{2}</email_reply_to>
  <email_subject>{3}</email_subject>
  <email_to>{4}</email_to>
  <email_encoding_xml />
  <queue_number>{5}</queue_number>
  <txn_id>{6}</txn_id>
  <unsubscribe_email />
  <email_attachment_xml>&lt;attachments /&gt;</email_attachment_xml>
  <send_email_blob_desc>initiatecampaignemail</send_email_blob_desc>
  <send_email_log_id>{7}</send_email_log_id>
</txn>";
    }
}
