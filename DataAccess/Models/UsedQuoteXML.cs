﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Models
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class used_quotes
    {

        private used_quotesUsed_vehicle_quote[] used_vehicle_quoteField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("used_vehicle_quote")]
        public used_quotesUsed_vehicle_quote[] used_vehicle_quote
        {
            get
            {
                return this.used_vehicle_quoteField;
            }
            set
            {
                this.used_vehicle_quoteField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class used_quotesUsed_vehicle_quote
    {

        private string vinField;

        private string stock_numberField;

        private uint vehicle_idField;

        private ushort yearField;

        private string makeField;

        private string modelField;

        private string manufacturer_codeField;

        private string trimField;

        private string autodata_trimField;

        private decimal invoiceField;

        private string msrpField;

        private string final_priceField;

        private string statusField;

        private string selection_reasonField;

        private string vehicle_blob_urlField;

        private string inventory_blob_urlField;

        private ushort responselogix_region_idField;

        private string source_inventory_urlField;

        private used_quotesUsed_vehicle_quoteImg[] imagesField;

        private object acodesField;

        private used_quotesUsed_vehicle_quoteOption[] categorized_optionsField;

        private uint customer_idField;

        private string customer_guidField;

        private string typeField;

        private string date_in_stockField;

        private string body_styleField;

        private string standard_bodyField;

        private byte doorsField;

        private string exterior_colorField;

        private string interior_colorField;

        private byte homenet_inventory_msrpField;

        private ushort homenet_inventory_final_priceField;

        private object monthly_lease_amountField;

        private object lease_down_paymentField;

        private object annual_mileageField;

        private object excess_mileage_feeField;

        private object lease_offer_expiration_dateField;

        private object lease_term_monthsField;

        private string lease_disclaimerField;

        private string photo_urlField;

        private string photo_1_urlField;

        private string photo_2_urlField;

        private string mileageField;

        private byte engine_cylindersField;

        private string engine_displacementField;

        private string transmissionField;

        private byte is_certifiedField;

        private object epa_cityField;

        private object epa_highwayField;

        private decimal wheel_baseField;

        private string engineField;

        private string rooftop_nameField;

        private string last_updated_dateField;

        private string optionsField;

        private string customer_inventory_sourceField;

        private object customer_inventory_quoted_priceField;

        private object customer_inventory_msrpField;

        private object price_after_markupField;

        private string vehicle_image_urlField;

        private string vehicle_image_1_urlField;

        private string vehicle_image_2_urlField;

        private string source_vehicle_urlField;

        private string make_pluralField;

        private string vehicle_numberField;

        private byte nhtsa_front_driver_valueField;

        private byte nhtsa_front_passenger_valueField;

        private byte nhtsa_side_rear_seat_valueField;

        private byte nhtsa_side_overall_side_valueField;

        private byte nhtsa_overall_valueField;

        /// <remarks/>
        public string vin
        {
            get
            {
                return this.vinField;
            }
            set
            {
                this.vinField = value;
            }
        }

        /// <remarks/>
        public string stock_number
        {
            get
            {
                return this.stock_numberField;
            }
            set
            {
                this.stock_numberField = value;
            }
        }

        /// <remarks/>
        public uint vehicle_id
        {
            get
            {
                return this.vehicle_idField;
            }
            set
            {
                this.vehicle_idField = value;
            }
        }

        /// <remarks/>
        public ushort year
        {
            get
            {
                return this.yearField;
            }
            set
            {
                this.yearField = value;
            }
        }

        /// <remarks/>
        public string make
        {
            get
            {
                return this.makeField;
            }
            set
            {
                this.makeField = value;
            }
        }

        /// <remarks/>
        public string model
        {
            get
            {
                return this.modelField;
            }
            set
            {
                this.modelField = value;
            }
        }

        /// <remarks/>
        public string manufacturer_code
        {
            get
            {
                return this.manufacturer_codeField;
            }
            set
            {
                this.manufacturer_codeField = value;
            }
        }

        /// <remarks/>
        public string trim
        {
            get
            {
                return this.trimField;
            }
            set
            {
                this.trimField = value;
            }
        }

        /// <remarks/>
        public string autodata_trim
        {
            get
            {
                return this.autodata_trimField;
            }
            set
            {
                this.autodata_trimField = value;
            }
        }

        /// <remarks/>
        public decimal invoice
        {
            get
            {
                return this.invoiceField;
            }
            set
            {
                this.invoiceField = value;
            }
        }

        /// <remarks/>
        public string msrp
        {
            get
            {
                return this.msrpField;
            }
            set
            {
                this.msrpField = value;
            }
        }

        /// <remarks/>
        public string final_price
        {
            get
            {
                return this.final_priceField;
            }
            set
            {
                this.final_priceField = value;
            }
        }

        /// <remarks/>
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public string selection_reason
        {
            get
            {
                return this.selection_reasonField;
            }
            set
            {
                this.selection_reasonField = value;
            }
        }

        /// <remarks/>
        public string vehicle_blob_url
        {
            get
            {
                return this.vehicle_blob_urlField;
            }
            set
            {
                this.vehicle_blob_urlField = value;
            }
        }

        /// <remarks/>
        public string inventory_blob_url
        {
            get
            {
                return this.inventory_blob_urlField;
            }
            set
            {
                this.inventory_blob_urlField = value;
            }
        }

        /// <remarks/>
        public ushort responselogix_region_id
        {
            get
            {
                return this.responselogix_region_idField;
            }
            set
            {
                this.responselogix_region_idField = value;
            }
        }

        /// <remarks/>
        public string source_inventory_url
        {
            get
            {
                return this.source_inventory_urlField;
            }
            set
            {
                this.source_inventory_urlField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("img", IsNullable = false)]
        public used_quotesUsed_vehicle_quoteImg[] images
        {
            get
            {
                return this.imagesField;
            }
            set
            {
                this.imagesField = value;
            }
        }

        /// <remarks/>
        public object acodes
        {
            get
            {
                return this.acodesField;
            }
            set
            {
                this.acodesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("option", IsNullable = false)]
        public used_quotesUsed_vehicle_quoteOption[] categorized_options
        {
            get
            {
                return this.categorized_optionsField;
            }
            set
            {
                this.categorized_optionsField = value;
            }
        }

        /// <remarks/>
        public uint customer_id
        {
            get
            {
                return this.customer_idField;
            }
            set
            {
                this.customer_idField = value;
            }
        }

        /// <remarks/>
        public string customer_guid
        {
            get
            {
                return this.customer_guidField;
            }
            set
            {
                this.customer_guidField = value;
            }
        }

        /// <remarks/>
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string date_in_stock
        {
            get
            {
                return this.date_in_stockField;
            }
            set
            {
                this.date_in_stockField = value;
            }
        }

        /// <remarks/>
        public string body_style
        {
            get
            {
                return this.body_styleField;
            }
            set
            {
                this.body_styleField = value;
            }
        }

        /// <remarks/>
        public string standard_body
        {
            get
            {
                return this.standard_bodyField;
            }
            set
            {
                this.standard_bodyField = value;
            }
        }

        /// <remarks/>
        public byte doors
        {
            get
            {
                return this.doorsField;
            }
            set
            {
                this.doorsField = value;
            }
        }

        /// <remarks/>
        public string exterior_color
        {
            get
            {
                return this.exterior_colorField;
            }
            set
            {
                this.exterior_colorField = value;
            }
        }

        /// <remarks/>
        public string interior_color
        {
            get
            {
                return this.interior_colorField;
            }
            set
            {
                this.interior_colorField = value;
            }
        }

        /// <remarks/>
        public byte homenet_inventory_msrp
        {
            get
            {
                return this.homenet_inventory_msrpField;
            }
            set
            {
                this.homenet_inventory_msrpField = value;
            }
        }

        /// <remarks/>
        public ushort homenet_inventory_final_price
        {
            get
            {
                return this.homenet_inventory_final_priceField;
            }
            set
            {
                this.homenet_inventory_final_priceField = value;
            }
        }

        /// <remarks/>
        public object monthly_lease_amount
        {
            get
            {
                return this.monthly_lease_amountField;
            }
            set
            {
                this.monthly_lease_amountField = value;
            }
        }

        /// <remarks/>
        public object lease_down_payment
        {
            get
            {
                return this.lease_down_paymentField;
            }
            set
            {
                this.lease_down_paymentField = value;
            }
        }

        /// <remarks/>
        public object annual_mileage
        {
            get
            {
                return this.annual_mileageField;
            }
            set
            {
                this.annual_mileageField = value;
            }
        }

        /// <remarks/>
        public object excess_mileage_fee
        {
            get
            {
                return this.excess_mileage_feeField;
            }
            set
            {
                this.excess_mileage_feeField = value;
            }
        }

        /// <remarks/>
        public object lease_offer_expiration_date
        {
            get
            {
                return this.lease_offer_expiration_dateField;
            }
            set
            {
                this.lease_offer_expiration_dateField = value;
            }
        }

        /// <remarks/>
        public object lease_term_months
        {
            get
            {
                return this.lease_term_monthsField;
            }
            set
            {
                this.lease_term_monthsField = value;
            }
        }

        /// <remarks/>
        public string lease_disclaimer
        {
            get
            {
                return this.lease_disclaimerField;
            }
            set
            {
                this.lease_disclaimerField = value;
            }
        }

        /// <remarks/>
        public string photo_url
        {
            get
            {
                return this.photo_urlField;
            }
            set
            {
                this.photo_urlField = value;
            }
        }

        /// <remarks/>
        public string photo_1_url
        {
            get
            {
                return this.photo_1_urlField;
            }
            set
            {
                this.photo_1_urlField = value;
            }
        }

        /// <remarks/>
        public string photo_2_url
        {
            get
            {
                return this.photo_2_urlField;
            }
            set
            {
                this.photo_2_urlField = value;
            }
        }

        /// <remarks/>
        public string mileage
        {
            get
            {
                return this.mileageField;
            }
            set
            {
                this.mileageField = value;
            }
        }

        /// <remarks/>
        public byte engine_cylinders
        {
            get
            {
                return this.engine_cylindersField;
            }
            set
            {
                this.engine_cylindersField = value;
            }
        }

        /// <remarks/>
        public string engine_displacement
        {
            get
            {
                return this.engine_displacementField;
            }
            set
            {
                this.engine_displacementField = value;
            }
        }

        /// <remarks/>
        public string transmission
        {
            get
            {
                return this.transmissionField;
            }
            set
            {
                this.transmissionField = value;
            }
        }

        /// <remarks/>
        public byte is_certified
        {
            get
            {
                return this.is_certifiedField;
            }
            set
            {
                this.is_certifiedField = value;
            }
        }

        /// <remarks/>
        public object epa_city
        {
            get
            {
                return this.epa_cityField;
            }
            set
            {
                this.epa_cityField = value;
            }
        }

        /// <remarks/>
        public object epa_highway
        {
            get
            {
                return this.epa_highwayField;
            }
            set
            {
                this.epa_highwayField = value;
            }
        }

        /// <remarks/>
        public decimal wheel_base
        {
            get
            {
                return this.wheel_baseField;
            }
            set
            {
                this.wheel_baseField = value;
            }
        }

        /// <remarks/>
        public string engine
        {
            get
            {
                return this.engineField;
            }
            set
            {
                this.engineField = value;
            }
        }

        /// <remarks/>
        public string rooftop_name
        {
            get
            {
                return this.rooftop_nameField;
            }
            set
            {
                this.rooftop_nameField = value;
            }
        }

        /// <remarks/>
        public string last_updated_date
        {
            get
            {
                return this.last_updated_dateField;
            }
            set
            {
                this.last_updated_dateField = value;
            }
        }

        /// <remarks/>
        public string options
        {
            get
            {
                return this.optionsField;
            }
            set
            {
                this.optionsField = value;
            }
        }

        /// <remarks/>
        public string customer_inventory_source
        {
            get
            {
                return this.customer_inventory_sourceField;
            }
            set
            {
                this.customer_inventory_sourceField = value;
            }
        }

        /// <remarks/>
        public object customer_inventory_quoted_price
        {
            get
            {
                return this.customer_inventory_quoted_priceField;
            }
            set
            {
                this.customer_inventory_quoted_priceField = value;
            }
        }

        /// <remarks/>
        public object customer_inventory_msrp
        {
            get
            {
                return this.customer_inventory_msrpField;
            }
            set
            {
                this.customer_inventory_msrpField = value;
            }
        }

        /// <remarks/>
        public object price_after_markup
        {
            get
            {
                return this.price_after_markupField;
            }
            set
            {
                this.price_after_markupField = value;
            }
        }

        /// <remarks/>
        public string vehicle_image_url
        {
            get
            {
                return this.vehicle_image_urlField;
            }
            set
            {
                this.vehicle_image_urlField = value;
            }
        }

        /// <remarks/>
        public string vehicle_image_1_url
        {
            get
            {
                return this.vehicle_image_1_urlField;
            }
            set
            {
                this.vehicle_image_1_urlField = value;
            }
        }

        /// <remarks/>
        public string vehicle_image_2_url
        {
            get
            {
                return this.vehicle_image_2_urlField;
            }
            set
            {
                this.vehicle_image_2_urlField = value;
            }
        }

        /// <remarks/>
        public string source_vehicle_url
        {
            get
            {
                return this.source_vehicle_urlField;
            }
            set
            {
                this.source_vehicle_urlField = value;
            }
        }

        /// <remarks/>
        public string make_plural
        {
            get
            {
                return this.make_pluralField;
            }
            set
            {
                this.make_pluralField = value;
            }
        }

        /// <remarks/>
        public string vehicle_number
        {
            get
            {
                return this.vehicle_numberField;
            }
            set
            {
                this.vehicle_numberField = value;
            }
        }

        /// <remarks/>
        public byte nhtsa_front_driver_value
        {
            get
            {
                return this.nhtsa_front_driver_valueField;
            }
            set
            {
                this.nhtsa_front_driver_valueField = value;
            }
        }

        /// <remarks/>
        public byte nhtsa_front_passenger_value
        {
            get
            {
                return this.nhtsa_front_passenger_valueField;
            }
            set
            {
                this.nhtsa_front_passenger_valueField = value;
            }
        }

        /// <remarks/>
        public byte nhtsa_side_rear_seat_value
        {
            get
            {
                return this.nhtsa_side_rear_seat_valueField;
            }
            set
            {
                this.nhtsa_side_rear_seat_valueField = value;
            }
        }

        /// <remarks/>
        public byte nhtsa_side_overall_side_value
        {
            get
            {
                return this.nhtsa_side_overall_side_valueField;
            }
            set
            {
                this.nhtsa_side_overall_side_valueField = value;
            }
        }

        /// <remarks/>
        public byte nhtsa_overall_value
        {
            get
            {
                return this.nhtsa_overall_valueField;
            }
            set
            {
                this.nhtsa_overall_valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class used_quotesUsed_vehicle_quoteImg
    {

        private string urlField;

        private byte orderField;

        /// <remarks/>
        public string url
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
            }
        }

        /// <remarks/>
        public byte order
        {
            get
            {
                return this.orderField;
            }
            set
            {
                this.orderField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class used_quotesUsed_vehicle_quoteOption
    {

        private string categoryField;

        private string descriptionField;

        /// <remarks/>
        public string category
        {
            get
            {
                return this.categoryField;
            }
            set
            {
                this.categoryField = value;
            }
        }

        /// <remarks/>
        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }
    }


}
