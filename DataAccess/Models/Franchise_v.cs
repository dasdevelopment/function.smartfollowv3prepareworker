﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataAccess.Models
{
    public class Franchise_v
    {
        public string customer_name { get; set; }
        public Guid customer_guid { get; set; }
        public string customer_zip { get; set; }
        public string make { get; set; }
        [Key]
        public int franchise_id { get; set; }
        public DateTime created_date { get; set; }
        public int customer_id { get; set; }
        public string alias_business_phone { get; set; }
        public string alias_mobile_phone { get; set; }
        public string lead_delivery_email { get; set; }
        public string status { get; set; }
        public string alias_consumer_phone { get; set; }
        public string alias_email_domain { get; set; }
        public int make_id { get; set; }
        public string business_phone { get; set; }
        public string lead_format { get; set; }
        public string lead_crm { get; set; }
        public bool multi_franchise_alias_creation { get; set; }
        public string website { get; set; }
        public string greeting_consumer_phone { get; set; }
        public Guid franchise_guid { get; set; }
        public string sales_force_url { get; set; }
        public string oem_vehicle_locator_logon { get; set; }
        public string oem_vehicle_locator_password { get; set; }
        public string oem_vehicle_locator_reference { get; set; }
        public bool non_aliased_campaigning { get; set; }
        public string campaigning_url { get; set; }
        public DateTime? status_reset_date { get; set; }
        public string oem_franchise_code { get; set; }
        public string facebook_url { get; set; }
        public string twitter_url { get; set; }
        public string youtube_url { get; set; }
        public bool send_quote_notification { get; set; }
        public string product_class { get; set; }
        public bool? send_no_quote_notification { get; set; }
        public DateTime? last_updated_date { get; set; }
        public string last_updated_by { get; set; }
    }
}
