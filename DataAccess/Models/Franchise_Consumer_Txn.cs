﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models
{
    public class Franchise_Consumer_Txn
    {
        [Key]
        public int Id { get; set; }
        public Guid txn_id { get; set; }
        public Guid franchise_consumer_id { get; set; }
    }
}
