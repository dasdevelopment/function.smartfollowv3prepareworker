﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models
{
    public class Smart_Follow_V3_Base_Defaults
    {
        [Key]
        public Guid default_template_values_id { get; set; }
        public string subject { get; set; }
        public string header_image_url { get; set; }
        public string header_image_redirect_url { get; set; }
        public string greeting_text { get; set; }
        public string intro_text { get; set; }
        public string outro_text { get; set; }
        public string signature_image_url_override { get; set; }
        public string signature_name_override { get; set; }
        public string signature_title_override { get; set; }
        public string signature_mobile_number_override { get; set; }
        public string signature_office_number_override { get; set; }
        public string signature_email_override { get; set; }
        public string signature_text_color_hex { get; set; }
        public string signature_accent_color_hex { get; set; }
        public string disclaimer_text { get; set; }
        public string border_color_hex { get; set; }
        public string link_color_hex { get; set; }
        public string link_visited_color_hex { get; set; }
        public string link_hover_color_hex { get; set; }
        public string link_active_color_hex { get; set; }
        public string button_color_hex { get; set; }
        public string button_text_color_hex { get; set; }
    }
}
