﻿using System;

namespace DataAccess.Models
{
    public class Franchise_Smart_Follow_v3_Schedule
    {
        public int franchise_id { get; set; }
        public int day { get; set; }
        public bool new_active { get; set; }
        public Guid? new_template_values_id { get; set; }
        public bool used_active { get; set; }
        public Guid? used_template_values_id { get; set; }

    }
}
