﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models
{
    public class Lead_History
    {
        [Key]
        public int id { get; set; }
        public Guid lead_history_id { get; set; }
        public Guid lead_id { get; set; }
        public DateTime history_date { get; set; }
        public string type { get; set; }
        public string logon { get; set; }
        public string description_1 { get; set; }
        public string description_2 { get; set; }
        public string description_3 { get; set; }
        public string description_4 { get; set; }
        public DateTime created_date { get; set; }
    }
}
