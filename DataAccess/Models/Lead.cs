﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models
{
    public class Lead
    {
        [Key]
        public Guid lead_id { get; set; }
        public int franchise_id { get; set; }
        public int customer_id { get; set; }
        public string first { get; set; }
        public string last { get; set; }
        public string email { get; set; }
        public string lp_year { get; set; }
        public string lp_make { get; set; }
        public string lp_model { get; set; }
        public string lp_trim { get; set; }
        public string lp_vin { get; set; }
        public string address_1 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string lp_franchise_contact_name { get; set; }
        public string lp_franchise_email { get; set; }
        public string type { get; set; }
        public string customer_name { get; set; }
        public string crm_reply_from { get; set; }
        public string forwarding_source { get; set; }
        public DateTime created_date { get; set; }
        public string full_name { get; set; }
        public string franchiseRepEmail { get; set; }
    }
}
