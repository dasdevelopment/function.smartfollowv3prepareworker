﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataAccess.Models
{
    public class Smart_Quote
    {
        public Guid smart_quote_id { get; set; }
        public DateTime created_date { get; set; }
        public Guid lead_id { get; set; }
        public string type { get; set; }
        public int? franchise_rep_id { get; set; }
        public string franchise_rep_name { get; set; }
        public string franchise_rep_title { get; set; }
        public string franchise_rep_email { get; set; }
        public string franchise_rep_business_phone { get; set; }
        public string franchise_rep_mobile_phone { get; set; }
        public string franchise_rep_photo_url { get; set; }
        public string franchise_rep_store { get; set; }
        public string alias_franchise_rep_email { get; set; }
        public string alias_franchise_rep_business_phone { get; set; }
        public string alias_franchise_rep_business_extension { get; set; }
        public string alias_franchise_rep_mobile_phone { get; set; }
        public string alias_franchise_rep_mobile_extension { get; set; }
        public bool? is_generic_franchise_rep { get; set; }
        public string franchise_reference { get; set; }
        public string email { get; set; }
        public string home_phone { get; set; }
        public string mobile_phone { get; set; }
        public string alias_email { get; set; }
        public string alias_home_phone { get; set; }
        public string alias_home_extension { get; set; }
        public string alias_mobile_phone { get; set; }
        public string alias_mobile_extension { get; set; }
        public int? vehicle_id { get; set; }
        public string make { get; set; }
        public string year { get; set; }
        public string model { get; set; }
        public string trim { get; set; }
        public string vin { get; set; }
        public string stock_number { get; set; }
        public string email_template_name { get; set; }
        public string quote_vehicle_xml { get; set; }
        public string used_vehicle_xml { get; set; }
        public bool is_opened { get; set; }
        public string raw_email { get; set; }
        public bool is_billable { get; set; }
        public DateTime smart_quote_date { get; set; }
        public string non_billable_type { get; set; }
        public string non_billable_logon { get; set; }
        public string raw_email_url { get; set; }
        public string new_vehicle_xml_url { get; set; }
        public string used_vehicle_xml_url { get; set; }
        public string email_template_text_name { get; set; }
        public string email_template_html_name { get; set; }
        public string web_template_name { get; set; }
        public string crm_reference { get; set; }
        public string crm_reply_to { get; set; }
        [Key]
        public int id { get; set; }
    }
}
