﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Models
{
    public class Email
    {
        public string HTMLBody { get; set; }
        public string EmailFrom { get; set; }
        public string EmailReplyTo { get; set; }
        public string EmailSubject { get; set; }
        public string EmailTo { get; set; }
    }
}
