﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models
{
    public class Franchise_Smart_Follow_V3_Defaults
    {
        [Key]
        public int franchise_id { get; set; }
        public Guid template_values_id { get; set; }
    }
}
