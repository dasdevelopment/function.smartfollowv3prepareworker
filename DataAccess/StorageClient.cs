﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Azure.Storage.Queues;
using DataAccess.Extensions;
using DataAccess.Models;
using Flurl.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public interface IStorageClient
    {
        public Task<string> SendSmartFollowEmail(Guid lead_id, Guid franchise_consumer_id, int franchise_id, int customer_id, string emailFrom, string emailTo, string emailReplyTo, string subject, string htmlBody);
        public Task<string> SendReQuote(Guid lead_id, DateTime quoteDate, Guid franchise_consumer_id, int franchise_id, int customer_id);
        public Task<string> SendCRMNotificationEmail(Guid lead_id, string emailFrom, string emailTo, string emailReplyTo, string subject, string htmlBody);
    }

    public class StorageClient : IStorageClient
    {
        private string _productionStorageRLConnectionString;

        public StorageClient(string productionStorageRLConnectionString)
        {
            _productionStorageRLConnectionString = productionStorageRLConnectionString;
        }

        public async Task SendEmail(Guid lead_id, string emailFrom, string emailTo, string emailReplyTo, string subject, string htmlBody)
        {
            var emailDetails = new Email()
            {
                EmailFrom = emailFrom,
                EmailTo = emailTo,
                EmailReplyTo = emailReplyTo,
                EmailSubject = subject,
                HTMLBody = htmlBody
            };

            var tempBlobUri = await CreateEmailTempBlob(lead_id, emailDetails);

            var queueName = "sendemail9queue";
            var queueMessage = string.Format(BaseValues.SendEmailQueueMessage,
                Guid.NewGuid(),
                tempBlobUri.AbsoluteUri,
                "9");

            await InsertQueueMessage(
                lead_id: lead_id,
                queueName: queueName,
                queueMessage: queueMessage);
        }

        public async Task<string> SendCRMNotificationEmail(Guid lead_id, string emailFrom, string emailTo, string emailReplyTo, string subject, string htmlBody)
        {
            try
            {
                var emailDetails = new Email()
                {
                    EmailFrom = emailFrom, // customer's email address
                    EmailTo = emailTo, // franchise rep's email address
                    EmailReplyTo = emailReplyTo, // customer's email address
                    EmailSubject = subject, // SmartFollow will be sent tomorrow!
                    HTMLBody = htmlBody
                };

                var tempBlobUri = await CreateEmailTempBlob(lead_id, emailDetails);

                var queueName = "sendemail9queue";
                var queueMessage = string.Format(BaseValues.SendEmailQueueMessage,
                    Guid.NewGuid(),
                    tempBlobUri.AbsoluteUri,
                    "9");

                await InsertQueueMessage(
                    lead_id: lead_id,
                    queueName: queueName,
                    queueMessage: queueMessage);

                return tempBlobUri.ToString();
            }
            catch
            {
                return null;
            }
        }

        public async Task<string> SendSmartFollowEmail(Guid lead_id, Guid franchise_consumer_id, int franchise_id, int customer_id, string emailFrom, string emailTo, string emailReplyTo, string subject, string htmlBody)
        {
            try
            {
                var emailDetails = new Email()
                {
                    EmailFrom = emailFrom,
                    EmailTo = emailTo,
                    EmailReplyTo = emailReplyTo,
                    EmailSubject = subject,
                    HTMLBody = htmlBody
                };

                var tempBlobUri = await CreateEmailTempBlob(lead_id, emailDetails);

                if(string.IsNullOrWhiteSpace(tempBlobUri.ToString()))
                {
                    return null;
                }

                var queueName = "sendemail9queue";
                var queueMessage = string.Format(BaseValues.SendEmailQueueMessage, Guid.NewGuid(), tempBlobUri.AbsoluteUri, "9");

                await InsertQueueMessage(
                    lead_id: lead_id,
                    queueName: queueName, 
                    queueMessage: queueMessage);

                await LogEvent(lead_id, "SmartFollowV3", "SmartFollowSent", "<txn></txn>");
                await LogSmartFollowReportTxn(lead_id, franchise_consumer_id, franchise_id, customer_id);
                return tempBlobUri.ToString();
            }
            catch
            {
                return null;
            }
        }

        public async Task<string> SendReQuote(Guid lead_id, DateTime quoteDate, Guid franchise_consumer_id, int franchise_id, int customer_id)
        {
            try
            {
                string container = $"queuelog-{quoteDate:yyyyMMdd}";
                string prefix = $"{lead_id.ToString().ToLower()}/";

                var queueLogBlobs = await GetBlobNamesForPrefix(
                    container: container,
                    prefix: prefix,
                    delimiter: "/");

                var smartQuoteSendLogBlobName = queueLogBlobs
                    .Where(blob => blob.Contains("sendemail1queue"))
                    .LastOrDefault();

                var smartQuoteQueueMessage = await GetBlobContents(container, smartQuoteSendLogBlobName);

                await InsertQueueMessage(
                    lead_id: lead_id,
                    queueName: "sendemail1queue",
                    queueMessage: smartQuoteQueueMessage);

                await LogSmartFollowReportTxn(lead_id, franchise_consumer_id, franchise_id, customer_id);

                return $"https://productionstoragerl.blob.core.windows.net/{container}/{smartQuoteSendLogBlobName}";
            }
            catch
            {
                return null;
            }
        }

        private async Task<Uri> CreateEmailTempBlob(Guid lead_id, Email emailDetails)
        {
            var blobText = BaseValues.SendEmailTempBlob;

            var formattedBlobText = string.Format(blobText, 
                SecurityElement.Escape(emailDetails.HTMLBody),
                emailDetails.EmailFrom,
                emailDetails.EmailReplyTo,
                emailDetails.EmailSubject,
                emailDetails.EmailTo,
                "9",
                lead_id,
                Guid.NewGuid());

            var blobUri = await CreateBlob(
                container: string.Format("temp-{0:yyyyMMdd}", GetPSTNow()),
                blobName: $"emailqueue.sendmessage.{Guid.NewGuid()}.xml",
                blobContent: formattedBlobText);

            return blobUri;
        }

        private static DateTime GetPSTNow()
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Pacific Standard Time");
        }

        public async Task LogEvent(Guid lead_id, string component, string logName, string logContent)
        {
            // efcf364d-cbcd-4649-9562-afad757739ca/0626409119982/log/ccw/QuoteOpenNotification+Send/*.*.*.*.*.xml
            //string blobName = $"{lead_id}/{GetPSTNow():HHmmssfffffff}/log/SmartFollowV3/SmartFollowSent.xml";

            string container = $"log-{GetPSTNow():yyyyMMdd}";
            string blobName = $"{lead_id.ToString().ToLower()}/{GetPSTNow():HHmmssfffffff}/log/{component}/{logName}.xml";

            await CreateBlob(
                container: container,
                blobName: blobName,
                blobContent: logContent);
        }

        // Used for SmartFacts reporting
        public async Task LogSmartFollowReportTxn(Guid lead_id, Guid franchise_consumer_id, int franchise_id, int customer_id)
        {
            var container = $"internal-{GetPSTNow():yyyyMMdd}";
            var blobName = $"smartfollowreporttxn/reporttxn.{customer_id}.{franchise_id}.{franchise_consumer_id}.{lead_id}.xml";

            await CreateBlob(
                container: container,
                blobName: blobName,
                blobContent: "<txn />");
        }

        public async Task LogQueue(Guid lead_id, string queueName, string queueMessage)
        {
            string container = $"queuelog-{GetPSTNow():yyyyMMdd}";
            string blobName = $"{lead_id.ToString().ToLower()}/{GetPSTNow():HHmmssfffffff}.{queueName}.{Guid.NewGuid()}.xml";

            await CreateBlob(
                container: container,
                blobName: blobName,
                blobContent: queueMessage);
        }

        private async Task<Uri> CreateBlob(string container, string blobName, string blobContent)
        {
            BlobServiceClient blobServiceClient = new BlobServiceClient(_productionStorageRLConnectionString);
            BlobContainerClient containerClient = blobServiceClient.GetBlobContainerClient(string.Format(container, GetPSTNow()));
            BlobClient blobClient = containerClient.GetBlobClient(blobName);

            var bytes = UTF8Encoding.Default.GetBytes(blobContent);
            using MemoryStream stream = new MemoryStream(bytes);

            await blobClient.UploadAsync(stream,
                    new BlobHttpHeaders
                    {
                        ContentType = "text/xml"
                    },
                    conditions: null)
                .ConfigureAwait(false);

            stream.Close();

            return blobClient.Uri;
        }

        private async Task InsertQueueMessage(Guid lead_id, string queueName, string queueMessage)
        {
            QueueClient queueClient = new QueueClient(_productionStorageRLConnectionString, queueName);
            await queueClient.CreateIfNotExistsAsync().ConfigureAwait(false);
            await queueClient.SendMessageAsync(queueMessage.ToBase64());

            await LogQueue(lead_id, queueName, queueMessage);
        }

        private async Task<List<string>> GetBlobNamesForPrefix(string container, string prefix, string delimiter)
        {
            BlobServiceClient blobServiceClient = new BlobServiceClient(_productionStorageRLConnectionString);
            BlobContainerClient containerClient = blobServiceClient.GetBlobContainerClient(container);

            var blobs = containerClient.GetBlobsByHierarchyAsync(prefix: prefix, delimiter: delimiter);

            List<string> blobNames = new List<string>();

            await foreach (var blob in blobs)
            {
                blobNames.Add(blob.Blob.Name);
            }

            return blobNames;
        }

        private async Task<string> GetBlobContents(string container, string blobName)
        {
            BlobServiceClient blobServiceClient = new BlobServiceClient(_productionStorageRLConnectionString);
            BlobContainerClient containerClient = blobServiceClient.GetBlobContainerClient(container);
            BlobClient blobClient = containerClient.GetBlobClient(blobName);

            if (await blobClient.ExistsAsync())
            {
                var response = await blobClient.DownloadAsync();
                using (var streamReader = new StreamReader(response.Value.Content))
                {
                    return await streamReader.ReadToEndAsync();
                }
            }

            return null;
        }
    }
}
