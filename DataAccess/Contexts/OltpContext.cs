﻿using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace DataAccess
{
    public class OltpContext : DbContext
    {
        private readonly string _connectionString;

        public OltpContext(DbContextOptions<OltpContext> options)
            : base(options)
        {

        }

        public OltpContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public DbSet<Franchise_Smart_Follow_v3_Schedule> franchise_smart_follow_v3_schedule { get; set; }
        public DbSet<Franchise_Smart_Follow_v3_Template_Values> franchise_smart_follow_v3_template_values { get; set; }
        public DbSet<Lead> lead { get; set; }
        public DbSet<Franchise_Consumer_Txn> franchise_consumer_txn { get; set; }
        public DbSet<Smart_Quote> smart_quote { get; set; }
        public DbSet<Vehicle_V> vehicle_v { get; set; }
        public DbSet<Franchise_v> franchise_v { get; set; }
        public DbSet<Customer> customer { get; set; }
        public DbSet<Franchise_PL> franchise_pl { get; set; }
        public DbSet<FranchiseRep> franchise_rep { get; set; }
        public DbSet<Smart_Follow_V3_Base_Defaults> smart_follow_v3_base_defaults { get; set; }
        public DbSet<Franchise_Smart_Follow_V3_Defaults> franchise_smart_follow_v3_defaults { get; set; }
        public DbSet<Lead_History> lead_history { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise_Smart_Follow_v3_Schedule>()
                .HasKey(c => new { c.franchise_id, c.day });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }
    }
}
