﻿using DataAccess;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using System;

[assembly: FunctionsStartup(typeof(Function.SmartFollowV3PrepareWorker.Startup))]
namespace Function.SmartFollowV3PrepareWorker
{
    class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddSingleton<IOltpClient>(new OltpClient(Environment.GetEnvironmentVariable("OltpConnectionString")));
            builder.Services.AddSingleton<IStorageClient>(new StorageClient(Environment.GetEnvironmentVariable("ProductionStorageRLConnectionString")));
        }
    }
}
