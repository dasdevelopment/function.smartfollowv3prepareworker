﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Function.SmartFollowV3PrepareWorker.Models
{
    public class NotificationQueueMessage
    {
        public Guid lead_id { get; set; }
        public int day { get; set; }
    }
}
