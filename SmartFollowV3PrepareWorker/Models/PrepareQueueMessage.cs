﻿using System;

namespace Function.SmartFollowV3PrepareWorker.Models
{

    public class PrepareQueueMessage
    {
        public Guid lead_id { get; set; }
        public string type { get; set; }
        public Guid? template_values_id { get; set; }
        public int day { get; set; }
        public bool send_smart_follow_email_notification { get; set; }
    }

}
