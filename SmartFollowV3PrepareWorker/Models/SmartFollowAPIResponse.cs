﻿using System;

namespace SmartFollowV3PrepareWorker.Models
{

    public class SmartFollowAPIResponse
    {
        public Data data { get; set; }
        public string html { get; set; }
    }

    public class Data
    {
        public string lead_id { get; set; }
        public Guid franchise_consumer_id { get; set; }
        public int franchise_id { get; set; }
        public int customer_id { get; set; }
        public string title { get; set; }
        public string link_color_hex { get; set; }
        public string link_visited_color_hex { get; set; }
        public string link_hover_color_hex { get; set; }
        public string link_active_color_hex { get; set; }
        public string border_color_hex { get; set; }
        public string button_color_hex { get; set; }
        public string button_text_color_hex { get; set; }
        public string signature_accent_color_hex { get; set; }
        public string signature_text_color_hex { get; set; }
        public string header_image_redirect_url { get; set; }
        public string header_image_url { get; set; }
        public string vehicle_image_url { get; set; }
        public string still_interested_button_redirect_url { get; set; }
        public string considering_other_vehicles_button_redirect_url { get; set; }
        public string already_purchased_button_redirect_url { get; set; }
        public string capital_one_banner_redirect_url { get; set; }
        public string greeting_text { get; set; }
        public string intro_text { get; set; }
        public string outro_text { get; set; }
        public string disclaimer_text { get; set; }
        public string rep_image { get; set; }
        public string rep_name { get; set; }
        public string rep_title { get; set; }
        public string rep_email { get; set; }
        public string rep_mobile_phone { get; set; }
        public string rep_office_phone { get; set; }
        public string dealer_name { get; set; }
        public string dealer_phone { get; set; }
        public string dealer_address { get; set; }
        public string customer_first { get; set; }
        public string customer_last { get; set; }
        public string customer_email { get; set; }
        public string days_since_lead { get; set; }
        public string months_since_lead { get; set; }
        public string requested_vehicle_year { get; set; }
        public string requested_vehicle_make { get; set; }
        public string requested_vehicle_model { get; set; }
        public string requested_vehicle_trim { get; set; }
        public string incentives_redirect_url { get; set; }
        public string test_drive_redirect_url { get; set; }
        public string unsubscribe_redirect_url { get; set; }
        public string privacy_policy_redirect_url { get; set; }
        public bool display_header_image { get; set; }
        public bool display_vehicle_image { get; set; }
        public bool display_rep_image { get; set; }
        public bool display_rep_title { get; set; }
        public bool display_rep_mobile_phone { get; set; }
        public bool display_rep_office_phone { get; set; }
        public bool display_rep_email { get; set; }
        public bool display_capital_one { get; set; }
    }

}
