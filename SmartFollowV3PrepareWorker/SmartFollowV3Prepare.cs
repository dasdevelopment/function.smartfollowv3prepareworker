using DataAccess;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Function.SmartFollowV3PrepareWorker.Models;
using SmartFollowV3PrepareWorker.Routines;
using System;
using System.Threading.Tasks;

namespace Function.SmartFollowV3PrepareWorker
{
    public class SmartFollowV3Prepare
    {
        private readonly IOltpClient _oltpClient;
        private readonly IStorageClient _storageClient;

        public SmartFollowV3Prepare(IOltpClient oltpClient, IStorageClient storageClient)
        {
            _oltpClient = oltpClient;
            _storageClient = storageClient;
        }

        [FunctionName("smart-follow-v3-prepare")]
        public async Task SmartFollowProcess([QueueTrigger("%PrepareQueueName%", Connection = "AzureWebJobsStorage")]string message, ILogger log)
        {
            log.LogInformation($"Processing message: {message}");
            var queueMessage = JsonConvert.DeserializeObject<PrepareQueueMessage>(message);

            // Follow re-quote routine
            if(queueMessage.template_values_id == new Guid("ceb59ee7-2f39-44e5-83b8-b3b2e91cf557"))
            {
                await ReQuote.Process(queueMessage, _oltpClient, _storageClient, log);
            }
            else
            {
                // Otherwise, follow the normal SmartFollow routine
                await SmartFollow.Process(queueMessage, _oltpClient, _storageClient, log);
            }
        }

        [FunctionName("smart-follow-v3-notification")]
        public async Task NotificationProcess([QueueTrigger("%NotificationQueueName%", Connection = "AzureWebJobsStorage")] string message, ILogger log)
        {
            log.LogInformation($"Processing message: {message}");
            var queueMessage = JsonConvert.DeserializeObject<NotificationQueueMessage>(message);

            await Notification.Process(queueMessage, _oltpClient, _storageClient, log);
        }
    }
}
