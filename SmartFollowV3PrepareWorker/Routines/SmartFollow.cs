﻿using DataAccess;
using Function.SmartFollowV3PrepareWorker.Extensions;
using Function.SmartFollowV3PrepareWorker.Functions;
using Function.SmartFollowV3PrepareWorker.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using SmartFollowV3PrepareWorker.Models;
using Microsoft.Azure.Documents;

namespace SmartFollowV3PrepareWorker.Routines
{
    public class SmartFollow
    {
        public static async Task Process(PrepareQueueMessage queueMessage, IOltpClient oltpClient, IStorageClient storageClient, ILogger log)
        {
            try
            {
                var response = await Environment.GetEnvironmentVariable("SmartFollowAPIDataEndpoint")
                    .WithHeaders(new
                    {
                        x_api_key = Environment.GetEnvironmentVariable("SmartFollowAPIKey")
                    })
                    .SetQueryParams(new
                    {
                        leadid = queueMessage.lead_id,
                        day = queueMessage.day,
                        template_values_id = queueMessage.template_values_id
                    })
                    .GetJsonAsync<SmartFollowAPIResponse>();

                var data = response.data;

                var blobUrl = await storageClient.SendSmartFollowEmail(
                    lead_id: queueMessage.lead_id,
                    franchise_consumer_id: data.franchise_consumer_id,
                    franchise_id: data.franchise_id,
                    customer_id: data.customer_id,
                    emailFrom: $"{data.dealer_name} - {data.rep_name} <{data.rep_email}>".EncodeForXML(),
                    emailTo: data.customer_email,
                    emailReplyTo: data.rep_email,
                    subject: data.title,
                    htmlBody: response.html);

                await oltpClient.LeadHistoryUpsert(
                    lead_id: queueMessage.lead_id,
                    desc1: $"SmartFollow 3.0 Sent - Day {queueMessage.day}",
                    desc2: blobUrl);

                if (queueMessage.send_smart_follow_email_notification)
                {
                    var escapedTemplate = response.html.Replace("\'", "\"");

                    var sfEmailblobUrl = await storageClient.SendCRMNotificationEmail(
                            lead_id: queueMessage.lead_id,
                            emailFrom: data.customer_email,
                            emailTo: data.rep_email,
                            emailReplyTo: data.customer_email,
                            subject: $"Smart Follow email sent to customer.",
                            htmlBody: $@"<html><body><p>Please see below for the Day {queueMessage.day} SmartFollow Email that sent to {data.customer_email} </p></body></html> {escapedTemplate}");

                    await oltpClient.LeadHistoryUpsert(
                        lead_id: queueMessage.lead_id,
                        desc1: $"SmartFollow 3.0 Email notification Sent to CRM - Day {queueMessage.day}",
                        desc2: sfEmailblobUrl);
                }


                log.LogWarning($"LIVE SMARTFOLLOW 3.0 SENT FOR {data.dealer_name} AT {HelperFunctions.GetPSTNow()} PST!  lead_id: {queueMessage.lead_id} Email Blob: {blobUrl}");
            }
            catch (FlurlHttpException ex)
            {
                var error = await ex.GetResponseJsonAsync<Error>();

                if (error != null)
                    log.LogError(error.error_body);
                else
                    log.LogError(ex.Message);
            }
            catch (Exception ex)
            {
                log.LogError(ex.Message);
            }
        }

        protected static string GetRLEmailFromAddress(string aliasEmailAddress)
        {
            try
            {
                if (!string.IsNullOrEmpty(aliasEmailAddress))
                {
                    var address = new MailAddress(aliasEmailAddress);

                    return $"sales@{address.Host}";
                }
            }
            
            catch(Exception x )
            {
                System.Diagnostics.Trace.TraceError(x.ToString() ); 
            }

            return null;
        }
    }
}
