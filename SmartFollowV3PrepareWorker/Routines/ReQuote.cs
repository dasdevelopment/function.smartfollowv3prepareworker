﻿using DataAccess;
using Function.SmartFollowV3PrepareWorker.Functions;
using Function.SmartFollowV3PrepareWorker.Models;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace SmartFollowV3PrepareWorker.Routines
{
    public class ReQuote
    {
        public static async Task Process(PrepareQueueMessage queueMessage, IOltpClient oltpClient, IStorageClient storageClient, ILogger log)
        {
            var leadTask = oltpClient.GetLead(queueMessage.lead_id).ConfigureAwait(false);
            var franchiseConsumerTxnTask = oltpClient.GetFranchiseConsumerTxn(queueMessage.lead_id).ConfigureAwait(false);
            var smartQuoteTask = oltpClient.GetSmartQuote(queueMessage.lead_id).ConfigureAwait(false);

            var lead = await leadTask;
            var franchiseConsumerTxn = await franchiseConsumerTxnTask;
            var smartQuote = await smartQuoteTask;

            if(smartQuote is object)
            {
                var blobUrl = await storageClient.SendReQuote(
                    lead_id: queueMessage.lead_id,
                    quoteDate: smartQuote.smart_quote_date,
                    franchise_consumer_id: franchiseConsumerTxn.franchise_consumer_id,
                    franchise_id: lead.franchise_id,
                    customer_id: lead.customer_id);

                if (!string.IsNullOrWhiteSpace(blobUrl))
                {
                    await oltpClient.LeadHistoryUpsert(
                        lead_id: lead.lead_id,
                        desc1: $"ReQuote Sent - Day {queueMessage.day}",
                        desc2: "");

                    log.LogWarning($"LIVE REQUOTE SENT FOR {lead.customer_name} AT {HelperFunctions.GetPSTNow()} PST!  lead_id: {lead.lead_id} Email Blob: {blobUrl}");
                }
            }
        }
    }
}
