﻿using DataAccess;
using Function.SmartFollowV3PrepareWorker.Functions;
using Function.SmartFollowV3PrepareWorker.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace SmartFollowV3PrepareWorker.Routines
{
    public static class Notification
    {
        public static async Task Process(NotificationQueueMessage queueMessage, IOltpClient oltpClient, IStorageClient storageClient, ILogger log)
        {
            try
            {
                var lead = await oltpClient.GetLead(queueMessage.lead_id).ConfigureAwait(false);

                if (lead is object)
                {
                    if(string.IsNullOrWhiteSpace(lead.email))
                    {
                        log.LogError($"Customer email is null or empty, cannot issue notification.");
                        return;
                    }

                    if (string.IsNullOrWhiteSpace(lead.franchiseRepEmail))
                    {
                        log.LogError($"Franchise rep email is null or empty, cannot issue notification.");
                        return;
                    }

                    var blobUrl = await storageClient.SendCRMNotificationEmail(
                        lead_id: queueMessage.lead_id,
                        emailFrom: lead.email,
                        emailTo: lead.franchiseRepEmail,
                        emailReplyTo: lead.email,
                        subject: $"SmartFollow campaign begins tomorrow",
                        htmlBody: $"<html><body>Manage SmartFollow settings for this lead by clicking <a href=\"https://extranet.responselogix.com/LeadMgmt.aspx?id={queueMessage.lead_id}\">here</a>.</body></html>");

                    if (!string.IsNullOrWhiteSpace(blobUrl))
                    {
                        await oltpClient.LeadHistoryUpsert(
                            lead_id: lead.lead_id,
                            desc1: $"SmartFollow day {queueMessage.day} CRM notification sent",
                            desc2: "");

                        log.LogWarning($"SMARTFOLLOW ACTIVATION NOTIFICATION SENT TO {lead.franchiseRepEmail} FOR {lead.customer_name} AT {HelperFunctions.GetPSTNow()} PST!  lead_id: {lead.lead_id} Email Blob: {blobUrl}");
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
