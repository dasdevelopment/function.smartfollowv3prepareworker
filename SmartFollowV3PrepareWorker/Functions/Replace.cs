﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Function.SmartFollowV3PrepareWorker.Functions
{
    public static class Replace
    {
        public static void SquareTag(string tag, string value, ref string baseTemplate)
        {
            baseTemplate = Regex.Replace(baseTemplate, $"\\[\\[\\[{tag}\\]\\]\\]", value);
        }

        public static void CurlyTag(string tag, string value, ref string baseTemplate)
        {
            baseTemplate = Regex.Replace(baseTemplate, "{{" + tag + "}}", value);
        }

        public static void CurlyTag(string[] tags, string value, ref string baseTemplate)
        {
            foreach (var tag in tags)
            {
                baseTemplate = Regex.Replace(baseTemplate, "{{" + tag + "}}", value);
            }
        }
    }
}
