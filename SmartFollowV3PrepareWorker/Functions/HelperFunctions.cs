﻿using System;

namespace Function.SmartFollowV3PrepareWorker.Functions
{
    public class HelperFunctions
    {
        public static DateTime GetPSTNow()
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Pacific Standard Time");
        }
    }
}
